import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String _agama = "Islam";
  String _jk = "";

  List<String> listAgama = [
    "Islam",
    "Kristen Protestan",
    "Kristen Katolik",
    "Hindu",
    "Budha"
  ];

  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerPass = new TextEditingController();
  TextEditingController controllerMotto = new TextEditingController();

  // fungsi pilih JK
  void pilihJk(String value) {
    setState(() {
      _jk = value;
    });
  }

  // fungsi pilih Agama
  void pilihAgama(String value) {
    setState(() {
      _agama = value;
    });
  }

  // fungsi kirim data
  void kirimData() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: [
            new Text('Nama Lengkap : ${controllerNama.text}'),
            new Text('Password : ${controllerPass.text}'),
            new Text('Motto Hidup : ${controllerMotto.text}'),
            new Text('Jenis Kelamin : ${_jk}'),
            new Text('Agama : ${_agama}'),
            new RaisedButton(
              child: new Text('OK'),
              color: Colors.teal,
              onPressed: () => Navigator.pop(context),
            ),
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        leading: new Icon(Icons.list),
        title: new Text("Data Diri"),
        backgroundColor: Colors.teal,
      ),
      body: new ListView(
        children: [
          new Container(
            padding: new EdgeInsets.all(10.0),
            child: new Column(
              children: [
                new TextField(
                  controller: controllerNama,
                  decoration: new InputDecoration(
                      hintText: 'Nama Lengkap',
                      labelText: 'Nama Lengkap',
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      )),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),
                new TextField(
                  controller: controllerPass,
                  obscureText: true,
                  decoration: new InputDecoration(
                      hintText: 'Password',
                      labelText: 'Password',
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      )),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),
                new TextField(
                  controller: controllerMotto,
                  maxLines: 3,
                  decoration: new InputDecoration(
                      hintText: 'Motto Hidup',
                      labelText: 'Motto Hidup',
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      )),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),
                new RadioListTile(
                  title: new Text('Laki-laki'),
                  value: 'laki-laki',
                  groupValue: _jk,
                  onChanged: (String? value) {
                    pilihJk(value!);
                  },
                  activeColor: Colors.blue,
                  subtitle: new Text('Pilih ini jika anda Laki-laki'),
                ),
                new RadioListTile(
                  title: new Text('Perempuan'),
                  value: 'perempuan',
                  groupValue: _jk,
                  onChanged: (String? value) {
                    pilihJk(value!);
                  },
                  activeColor: Colors.blue,
                  subtitle: new Text('Pilih ini jika anda perempuan'),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                ),
                new Row(
                  children: [
                    new Text(
                      'Agama',
                      style: new TextStyle(fontSize: 18.0, color: Colors.blue),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(left: 15.0),
                    ),
                    DropdownButton(
                      onChanged: (String? value) {
                        pilihAgama(value!);
                      },
                      value: _agama,
                      items: listAgama.map((String value) {
                        return new DropdownMenuItem(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                    ),
                  ],
                ),
                new RaisedButton(
                  child: new Text('OK'),
                  color: Colors.blue,
                  onPressed: () {
                    kirimData();
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
