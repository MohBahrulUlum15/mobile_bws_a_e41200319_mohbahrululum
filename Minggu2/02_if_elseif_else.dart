import 'dart:io';

void main(List<String> args) {
  print("Welcome to Game Werewolf");
  print("Masukkan nama anda!");
  const clue = "Selamat datang di Dunia Werewolf";
  var nama;
  nama = stdin.readLineSync()!;
  var roles = " - penyihir\n - guard\n - werewolf";
  if (nama == "") {
    print("Nama harus diisi!");
  } else {
    print("Pilih role anda!" + "\n" + roles);
    String role = stdin.readLineSync()!;
    if (role == "") {
      print("Hello " + nama + ", pilih role untuk mulai bermain!");
    } else if (role == "penyihir") {
      print(
          "$clue, $nama \nHalo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (role == "guard") {
      print(
          "$clue, $nama \nHallo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (role == "werewolf") {
      print(
          "$clue, $nama \nHalo Werewolf $nama, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Role tidak ditemukan!");
    }
  }
}
