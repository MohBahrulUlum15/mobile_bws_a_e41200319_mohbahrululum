void main(List<String> args) {
  print('LOOPING PERTAMA');
  var i = 2;
  while (i <= 20) {
    if (i % 2 == 0) {
      print('$i - I LOVE CODING');
    }
    i++;
  }
  print('LOOPING KEDUA');
  var j = 20;
  while (j >= 2) {
    if (j % 2 == 0) {
      print('$j - I WILL BECOME MOBILE PROGRAMER');
    }
    j--;
  }
}
