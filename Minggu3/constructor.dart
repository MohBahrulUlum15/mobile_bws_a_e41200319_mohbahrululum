void main(List<String> args) {
  var idEmployee = new Employee.id(41200319);
  var nameEmployee = new Employee.name('Moh. Bahrul Ulum');
  var departementEmployee = new Employee.departement('Teknologi Informasi');

  print('ID : ${idEmployee.id}');
  print('Name : ${nameEmployee.name}');
  print('Departement : ${departementEmployee.departement}');
}

class Employee {
  int? id;
  String? name;
  String? departement;

  Employee.id(this.id);
  Employee.name(this.name);
  Employee.departement(this.departement);
}
