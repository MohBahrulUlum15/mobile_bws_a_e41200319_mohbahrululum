void main(List<String> args) {
  tampilkan();
  print(munculangka());
  print(kalikanDua(6));
  print(kalikan(5, 6));
  tampilkanAngka(5);
  print(functionPerkalian(5, 6));
}

// contoh 1 --- function sederhana tanpa return
tampilkan() {
  print("Hello Peserta Bootcamp");
}

// contoh 2 --- function sederhana dengan return
munculangka() {
  return 2;
}

// contoh 3 --- function dengan parameter
kalikanDua(angka) {
  return angka * 2;
}

// contoh 4 --- pengiriman parameter lebih dari satu
kalikan(x, y) {
  return x * y;
}

// contoh 5 --- inisialisasi parameter dengan nilai default
tampilkanAngka(n1, {s1: 45}) {
  print(n1); //hasil sesuai parameter n1
  print(s1); //hasil 45 karena parameter defaultnya adalah 45
}

//Kita juga dapat menampung function sebagai variable dengan sebuah bentuk function yang dinamakan Anonymous Functions
functionPerkalian(angka1, angka2) {
  return angka1 * angka2;
}
