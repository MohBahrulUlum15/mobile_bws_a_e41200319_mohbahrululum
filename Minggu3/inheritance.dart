import 'dart:ffi';

void main(List<String> args) {
  armor_titan armorTitan = new armor_titan();
  attack_titan attackTitan = new attack_titan();
  beast_titan beastTitan = new beast_titan();
  human humannn = new human();

  armorTitan.power = 4;
  attackTitan.power = 9;
  beastTitan.power = 6;
  humannn.power = 12;

  print(armorTitan.terjang() + "\n ${armorTitan.power}");
  print(attackTitan.punch() + "\n ${attackTitan.power}");
  print(beastTitan.lempar() + "\n ${beastTitan.power}");
  print(humannn.killAlltitan() + "\n ${humannn.power}");
}

class armor_titan extends titan {
  String terjang() => "woi...terjang";
}

class attack_titan extends titan {
  String punch() => "blam...blam";
}

class beast_titan extends titan {
  String lempar() => "wush...wush";
}

class human extends titan {
  String killAlltitan() => "Sasageyo ... Shinzo Sasageyo...";
}

class titan {
  late int powerPoint;

  int get power => powerPoint;
  set power(int value) {
    if (value <= 5) {
      value = 5;
    }
    powerPoint = value;
  }
}
