void main(List<String> args) {
  BangunDatar bangunDatar = new BangunDatar();
  Lingkaran lingkaran = new Lingkaran(14, 3.14);
  Persegi persegi = new Persegi(8);
  Segitiga segitiga = new Segitiga(3, 4, 5);

  bangunDatar.luas();
  bangunDatar.keliling();

  print("Luas lingkaran : ${lingkaran.luas()}");
  print("Keliling lingkaran : ${lingkaran.keliling()}");
  print("Luas persegi : ${persegi.luas()}");
  print("Keliling persegi : ${persegi.keliling()}");
  print("Luas segitiga : ${segitiga.luas()}");
  print("Keliling segitiga : ${segitiga.keliling()}");

}

//Class Bangun Datar
class BangunDatar {
  double luas() {
    print("Menghitung Luas Bangun Datar");
    return 0.0;
  }

  double keliling() {
    print("Menghitung Keliling Bangun Datar");
    return 0.0;
  }
}

//Class Persegi
class Persegi extends BangunDatar {
  late double sisi;
  Persegi(double sisi) {
    this.sisi = sisi;
  }

  @override
  double luas() {
    return sisi * sisi;
  }

  @override
  double keliling() {
    return 4 * sisi;
  }
}

//Class Lingkaran
class Lingkaran extends BangunDatar {
  late double phi, jari;

  Lingkaran(double jari, double phi) {
    this.jari = jari;
    this.phi = phi;
  }

  @override
  double luas() {
    return phi * jari * jari;
  }

  @override
  double keliling() {
    return 2 * phi * jari;
  }
}

//Class Segitiga
class Segitiga extends BangunDatar {
  late double alas, tinggi, sisiMiring;

  Segitiga(double alas, double tinggi, double sisiMiring) {
    this.alas = alas;
    this.tinggi = tinggi;
    this.sisiMiring = sisiMiring;
  }

  @override
  double luas() {
    return (alas * tinggi) / 2;
  }

  @override
  double keliling() {
    return alas + tinggi + sisiMiring;
  }
}
