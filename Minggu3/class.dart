// Mengubah Prosedural ke Class

//---PROSEDURAL---//
// void main(List<String> args) {
//   double setengah, alas, tinggi;
//   setengah = 0.5;
//   alas = 20.0;
//   tinggi = 30.0;

//   var luasSegitiga = setengah * alas * tinggi;

//   print(luasSegitiga);
// }

//---Class dan Object---//
void main(List<String> args) {
  Segitiga segitiga = new Segitiga();
  segitiga.setengah = 0.5;
  segitiga.alas = 6;
  segitiga.tinggi = 8;
  print(segitiga.hitungLuas());
}

// Class dan Object
class Segitiga {
  double setengah = 0.0;
  double alas = 0.0;
  double tinggi = 0.0;

  double hitungLuas() {
    return setengah * alas * tinggi;
  }
}
