void main(List<String> args) {
  Lingkaran lingkaran = new Lingkaran();
  lingkaran.setPhi(3.14);
  lingkaran.setJari(7);
  double luasLingkaran = lingkaran.hitungLuas();
  print(luasLingkaran);
}

class Lingkaran {
  late double phi;
  late double jari;
  void setPhi(double value) {
    if (value < 0) {
      value *= -1;
    }
    phi = value;
  }

  double getPhi() {
    return phi;
  }

  void setJari(double value) {
    if (value < 0) {
      value *= -1;
    }
    jari = value;
  }

  double getLebar() {
    return jari;
  }

  double hitungLuas() {
    return this.phi * jari;
  }
}
